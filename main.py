import extra

# Main Program

"""
Parse arguements
"""
arguements = extra.parse_args()

tor = arguements[0]
nodefault = arguements[1]
exclude_list = arguements[2]
domain = arguements[3]

"""
Grab website information 
"""
tor = False # Don't use tor
history = extra.request("https://viewdns.info/iphistory/?domain=" + domain,tor)

history = extra.cleanup(history,"td") # Cleanup our html

# Cleanup html more (remove site text)

# Remove uneeded items
history = extra.remove_excess(history)

# Add whitespace
history = extra.add_whitespace(history)
"""
Setup exclusions (e.g exclude cloudflare from ip history)
"""
exclude = ["cloudflare"]

# Set exclude to none if nodefault selected
if nodefault == True:
    exclude = []

# Added extra exclusions
exclude_list = exclude_list.split(",")
exclude += exclude_list
"""
Exclude and latest ip
"""
history = extra.remove_excludes(history,exclude)

# Latest IP
latest_ip = []
for x in history:
    exit = False
    # This code has to be smelly :(
    if x == "":
        exit = True
        break
    if exit:
        break
    latest_ip.append(x)
"""
Output
"""
for x in history:
    print(x)

print("#---Latest IP---#")
for x in latest_ip:
    print(x)
