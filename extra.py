# Extra
from bs4 import BeautifulSoup # Cleanup and search our html
import requests # Request our html
import argparse # Parse arguements

"""
Subroutines for parsing arguements
"""
# Take in command line arguements
def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('-t', '-tor', dest='tor', action="store_true", default=False) # True or False, default is False
    parser.add_argument('-nd', '-nodefault', dest='nodefault', action="store_true", default=False) # True or False, default is False
    parser.add_argument('-d', '--domain', dest='domain', action="store", required=True) # Take the domain (required)
    parser.add_argument('-e', '--exclude', dest='exclude', action="store") # Take in a list of providers seperated by commas
    
    args = parser.parse_args()
    return(args.tor,args.nodefault,args.exclude,args.domain) # Return arguements in a tuple
"""
Subroutines for grabbing website information
"""
# Send requests to websites
def request(url,tor):
    headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}
    if tor == True:
        # Tor (Hide address from requests)
        proxies = {
	    'http': 'socks5://127.0.0.1:9050',
	    'https': 'socks5://127.0.0.1:9050'
	    }
    else:
         proxies = {}

    # Catch any errors and return
    try:
        request = requests.get(url,proxies=proxies,headers=headers).text # Make request
        return(request)
    except:
        if tor == True:
            # If tor service is not started
            raise ValueError("ERROR: Is the tor service started?")
        else:
            raise ValueError("ERROR: Unknown")

# Filter our html
def cleanup(html,find):
    soup = BeautifulSoup(html,'lxml')

    tags = []
    tag = soup.find_all(find) # find our tag
    for x in tag:
        tags.append(x.text.strip()) # append our tag to tags and remove whitespace
    
    return(tags) # Return filtered html

# Remove uneeded items
def remove_excess(list):
	# Cleanup start of list
	list.reverse()
	count = 0 # start count
	while count < 9:
	    list.pop() # pop first items in list (because list is reversed
	    count = count + 1 
	list.reverse() # unreverse list

	# Cleanup end of list
	list.pop()
	list.pop()

	return(list) # Return list without uneeded items

# Remove whitespace
def add_whitespace(list):
	count = 1
	new_list = []
	# if it is in 4 times table add whitespace
	for x in list:
		if count % 4 == 0:
			new_list.append(x)
			new_list.append("")
			count = count + 1
		else:
			new_list.append(x)
			count = count + 1

	return(new_list) # return list with whitespace

# Remove excludes from domain history
def remove_excludes(history,exclude):
    for x in history:
        x_lower = x.lower() # Match two as lowercase
        for excludes in exclude:
            excludes = excludes.lower()
            if excludes in x_lower:
                index = history.index(x)
                del history[index]
    for x in history:
        if x == "":
            index = history.index(x)
            if history[index-4] == "":
                for x in range(0,4):
                    del history[index-3]
            # get place of x and figure out if there is whitespace there
    return(history)
